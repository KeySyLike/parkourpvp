<?php

namespace pvp;

use pocketmine\command\Command;
use pocketmine\event\block\BlockBreakEvent;
use pocketmine\event\Listener;
use pocketmine\event\player\PlayerChatEvent;
use pocketmine\level\Level;
use pocketmine\plugin\PluginBase;
use pvp\arena\Arena;
use pvp\commands\ParkourPVPCommand;
use pvp\math\Vector3;
use pvp\provider\YamlDataProvider;

/**
 * Class ParkourPvP
 * @package pvp
 */
class Main extends PluginBase implements Listener {

    /** @var YamlDataProvider */
    public $dataProvider;

    /** @var Command[] $commands */
    public $commands = [];

    /** @var Arena[] $arenas */
    public $arenas = [];

    /** @var Arena[] $setters */
    public $setters = [];

    /** @var int[] $setupData */
    public $setupData = [];

    public function onEnable() {
        $this->getServer() -> getPluginManager()->registerEvents($this, $this);
        $this->dataProvider = new YamlDataProvider($this);
        $this->getServer()->getCommandMap()->register("ParkourPvP", $this->commands[] = new ParkourPVPCommand($this));
    }
    
    public function onDisable() {
        $this->dataProvider->saveArenas();
    }
} 